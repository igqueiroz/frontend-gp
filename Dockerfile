# Dockerfile extende uma Imagem genérica em node para o GAE
FROM gcr.io/google_appengine/nodejs
# NodeJs 8 das imagens pré-mobuladas do Google
RUN /usr/local/bin/install_node 8
COPY . /app/

RUN apt-get update

# Expose the port to run the env
EXPOSE 8080


# npm install
RUN npm install --unsafe-perm || \
  ((if [ -f npm-debug.log ]; then \
      cat npm-debug.log; \
    fi) && false)
# roda a build da última versão
RUN npm run build

# roda o SSR e fica em modo de listen do servidor
CMD npm run start:ssr

