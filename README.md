# Front-End GP

## Table of Contents and Commands

Using Create React App, Express, SASS (SCSS), Redux, React BootStrap, React Router, Docker-Compose to the best dev environment and Dockerfile to run on the cloud.

- Run the app (localhost:3000)
npm install
npm start

- Build the last version 
npm run build

- Start the production env with SSR (localhost:3001)
npm run start:ssr

- Upload files on Google Storage - Google App Engine with Flex Env
gcloud app deploy

- Docker Build
docker-compose build -t frontend-up .

- Docker Run (on the brackets put your local folder location)
docker run -p 8889:3000  -v [frontend-up/]:/usr/src/app frontend-up

- Docker-Compose Up - Dev Env para a nuvem (localhost:9000)
docker-compose up