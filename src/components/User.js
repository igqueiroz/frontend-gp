// Componente que lê os dados dos usuários do App usando o Fetch direto no componente
import React, { Component } from 'react';
import DataApi  from '../logic/DataApi';
import UsersItem from './UsersItem';


export default class User extends Component {
	constructor(props) {
        super(props);
        this.state = {
            user: {},
        };
    }

	componentWillMount() {
		fetch('https://api.github.com/users/igqueiroz')
            .then(response => {
                if(response.ok) {
                    return response.json()
                }
                else {
                    throw new Error("Não rolou comunicação com a API");
                }
            })
            .then(list => {
				this.setState({
					user: {
						name: list.name,
						url: list.url
					}
				});
			}); 
    }
      
	

	render() {
		return(
			<div className="user">
				<h1>{this.state.user.name}</h1>
				
				<h3>{this.state.user.url}</h3>
			</div>
		)
	}
}