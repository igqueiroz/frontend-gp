// Arquivo de exemplo de texto comum sendo acessado pelo router

import React, {Component}  from 'react';
import { Link } from 'react-router';

export default class GymPass extends Component {

	render() {
		
		return(
				<section>
				<div className="logo"><Link to="/" title="GymPass App" alt="GymPass App"><img src="images/logo.jpg" alt="Laureate International Universities" title="GymPass - GitHub" /></Link></div>
				<div className="row">
					<div className="col-sm-3" />
						<div className="col-sm-6">
						<h1>About GymPass</h1>
						<p>GymPass is a platform that offers unrivaled access to gyms, health clubs and niche fitness studios. GymPass will provide pay-as-you-go access to our network of gyms by offering an assortment of on-demand passes for purchase. GymPass offers the flexibility to use multiple gym locations providing greater access to amenities, the freedom to work out with friends and the ability to pursue your personal fitness goals. No contracts. No long-term commitments. No initiation fees. Sign up today!
						</p>
					</div>
					<div className="col-sm-3" />
				</div>
				</section>
		)
	}
}

