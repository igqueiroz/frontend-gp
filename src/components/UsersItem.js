import React, { Component } from 'react';

export default class UsersItem extends Component {
    render(){
        return (
        		<div className="repos">
					<a href={this.props.link}>{this.props.name}</a>
					<p>{this.props.description}</p>
					<br/><br/>
				</div>
        );
    }
}



