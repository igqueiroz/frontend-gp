// Container Component que faz integração do Redux
import React, { Component } from 'react';
import { Link } from 'react-router';
import DataApi  from '../logic/DataApi';
import UsersItem from './UsersItem';
import User from './User';


export default class Github extends Component {
	constructor(props) {
        super(props);
        this.state = {
            repos: []
        };
    }

	componentWillMount() {
		this.props.routes[0].store.subscribe(() => {
			this.setState({
				repos: this.props.routes[0].store.getState()
			});
		})
		this.props.routes[0].store.dispatch(DataApi.list(`https://api.github.com/users/igqueiroz/repos`));
	}

	render() {
		return(
			<section>
				<div className="logo"><Link to="/" title="GymPass App" alt="GymPass App">
					<img src="images/logo.jpg" alt="GymPass" title="GymPass" /></Link>
				</div>
				<div className="container">
				<div className="row">
					<User store={this.props.routes[0].store}  />
					
					<h1>Repos</h1>
					{
						this.state.repos.map(rep =>
						<UsersItem 
							key={rep.id} 
							name={rep.name}
							link={rep.html_url}
							description={rep.description}
						/>)
					}
				</div>
				</div>
			</section>
		)
	}
}