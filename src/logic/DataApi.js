// Lógicas do App, envia as requisições aos reducers e mantém as views focadas na exibição de conteúdo

export default class DataApi {
    
    // Lista os dados
    static list(listUrl){
      return dispatch => {  
        const requestInfo = {
          method: 'GET',
          mode: 'cors'
        };
        //recebe os dados consumíveis da API
        fetch(listUrl, requestInfo)
            .then(response => {
                if(response.ok) {
                    return response.json()
                }
                else {
                    throw new Error("Não rolou comunicação com a API");
                }
            })
            .then(list => {
              //envia a requisição para o reducer do Redux e realiza a ação desejada
              const listing = list
              // O "listing" do reducer deve retornar a função manipulada para devolver um Array ao nosso componente
              dispatch({type:'LISTDATA', listing});
              console.log(listing)
              return listing;
          }); 

      }

    }
    // Lista os dados
    static listUser(listUrl){
      return dispatch => {  
        const requestInfo = {
          method: 'GET',
          mode: 'cors'
        };
        //recebe os dados consumíveis da API
        fetch(listUrl, requestInfo)
            .then(response => {
                if(response.ok) {
                    return response.json()
                }
                else {
                    throw new Error("Não rolou comunicação com a API");
                }
            })
            .then(list => {
              //envia a requisição para o reducer do Redux e realiza a ação desejada
              const listing = list
              // O "listing" do reducer deve retornar a função manipulada para devolver um Array ao nosso componente
              dispatch({type:'LISTUSER', listing});
              console.log(listing)
              return listing;
          }); 

      }

    }
}

